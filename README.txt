

*************************
LightCRM Module Functions
*************************

- Provides a simple *very basic* Customer Relationship management system using the comments module
- Notifies user via email of reply from admin
- emails reply of admin to user
- When user logs in, the lightcrm block will check for any new replies from admin and will display a link to the reply.



**************
The long Story
**************

The module allows for a very basic way of communicating with users. It adds a LightCrm tab to the user menu. A user posts a comment/support request by c;icking on the LightCrm tab and admin can view the user comments by clicking on the comments menu. The module creates a new node type called lightcrm and creates a new user block for lightcrm.

Enjoy!

*************
Installation
*************


Simply drop the module into the Drupal modules folder, and activate via the admin panel as normal. No database installation required.
Usage:
Navigate to settings -> lightcrm to select which roles receive notifications of new support requests.
Configure email message using the available tags
Select if users receive a notice everytime admin posts a reply